from django import forms
from django.forms.widgets import CheckboxSelectMultiple

FAVORITE_SUBJECT_CHOICES = [
    ('MPKTB', 'MPKT B'),
    ('DDP1', 'DDP 1'),
    ('MATDAS', 'MATDAS'),
    ('FISDAS', 'FISDAS'),
    ('PSD', 'PSD'),
    ('PPW', 'PPW'),
    ('ALIN', 'ALIN'),
]

class FavSubject(forms.Form):
    favorite_subject = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=FAVORITE_SUBJECT_CHOICES,
    )