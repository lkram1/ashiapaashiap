
# Create your views here.
from django.shortcuts import render, redirect

# Import forms & models
from .forms import BiodataForm
from .models import Biodata

# Create your views here.
def biodata_page(request):
    context = {}

    if request.method == 'POST':
        form = BiodataForm(request.POST)
        if form.is_valid():
            form.save() # versi 1
            # models.Biodata(**form.cleaned_data).save() # versi 2
            return redirect('biodata:biodata_page')
    else:
        form = BiodataForm()

    context['form'] = form
    context['biodatas'] = Biodata.objects.all()

    return render(request, 'biodata/biodata.html', context)