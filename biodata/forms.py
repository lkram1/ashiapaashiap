from django import forms

from .models import Biodata

class BiodataForm(forms.ModelForm):
    class Meta:
        model = Biodata
        fields = "__all__"
        widgets = {
            'birthday': forms.DateInput(
                attrs={'type' : 'date'}
            ),
        }