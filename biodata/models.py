from django.db import models

# Create your models here.

# Create your models here.
class Biodata(models.Model):
    full_name = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    age = models.PositiveSmallIntegerField()
    birthday = models.DateField(auto_now=False, auto_now_add=False)
    address = models.CharField(max_length=255)
    education = models.CharField(max_length=255)
    hobby = models.CharField(max_length=255)
    favorite_food = models.CharField(max_length=255)
    favorite_subject = models.CharField(max_length=255)
    number_of_siblings = models.PositiveSmallIntegerField()